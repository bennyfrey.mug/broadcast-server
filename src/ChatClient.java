import java.io.*;
import java.net.*;
public class ChatClient {
	private static final int PORT = 1234;
	private static Socket link;
	private static BufferedReader in;
	private static PrintWriter out;
	private static BufferedReader kbd;

	public static void main(String[] args) throws Exception{
		try{
			link = new Socket("127.0.0.1", PORT);
			in = new BufferedReader(new InputStreamReader(link.getInputStream()));
			out = new PrintWriter(link.getOutputStream(), true);
			kbd = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Connected to the chat server\n");

			// This will handle the response to other users
			ResponseHandler responseHandler = new ResponseHandler(link);
			responseHandler.start();

			String message = "", ClientName;
			System.out.print("Enter name: ");
			// first entry assume we get the name of the client
			ClientName = kbd.readLine();
			out.println(ClientName);

			do{

				System.out.print("Enter message (BYE to quit): ");
				message = kbd.readLine();
				out.println(message);

			}while (!message.equals("BYE"));
			System.out.print("Enter message (BYE to quit): ");
		}
		catch(UnknownHostException e){System.exit(1);}
		catch(IOException e){System.exit(1);}
		finally{
			try{
				if (link!=null){
					System.out.println("Connection shut down");
					link.close();
				}
			}
			catch(IOException e){System.exit(1);}
		}
	}//end main
}//end class MultiEchoClient

/**
 * Class that extends Thread to help with the output
 * without hanging from the keyboard
 */
class ResponseHandler extends Thread {
	private Socket client;
	private BufferedReader in;
	private PrintWriter out;

	public ResponseHandler(Socket socket){
		client = socket;
		try{
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			out = new PrintWriter(client.getOutputStream(), true);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * This helps to output the message without hanging from the keyboard
	 */
	@Override
	public void run() {
		try {
			while (!client.isClosed()) {
				try {
					String response = in.readLine();
					System.out.print("\n"+ response + "\n" + "Enter message (BYE to quit): ");
				} catch (SocketException ignored) {
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				client.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
	
	
