//This is a support class that extends Thread, runs the client thread
//and sends and receives messages

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class ClientHandler extends Thread{
  private Socket client;
  private BufferedReader in;
  private PrintWriter out;

  public ClientHandler(Socket socket){
    client = socket;
    try{
      in = new BufferedReader(new InputStreamReader(client.getInputStream()));
      out = new PrintWriter(client.getOutputStream(), true);
    }
    catch(IOException e){
      e.printStackTrace();
    }
  }
  public void run(){
    try{

      String received;
      received = in.readLine();
      // first line assume we get the name add to list
      setName(received);
      BroadcastChatServer.connectedClients.add(this);
      announceJoin(BroadcastChatServer.connectedClients);

      // stop when client sends BYE
      do{
        received = in.readLine();
        broadCastMessage(received, BroadcastChatServer.connectedClients);
      } while (!received.equals("BYE"));
    }
    catch(IOException e){
      e.printStackTrace();
    }
    finally{
      try{
        if(client!=null){
          System.out.println("Closing down connection...");
          client.close();
        }
      }
      catch(IOException e){
        e.printStackTrace();
      }
    }
  }//end run

  /**
   * This method announces a joined client to other connected clients
   * @param clients list of all connected clients
   * @throws IOException
   */
  public void announceJoin(ArrayList<ClientHandler> clients) throws IOException {
    if (clients != null) {
      for (ClientHandler clientHandler : clients) {
        if (getId() != clientHandler.getId()) {
          clientHandler.out.println(getName() + " has joined");
        }
      }
    }
    System.out.println(getName()+" has joined");
  }

  /**
   * The method is used to send message to other connected clients
   * excluding the one sending
   * @param received String the message to send to clients
   * @param clients list of all connected clients
   * @throws IOException
   */
  public void broadCastMessage(String received, ArrayList<ClientHandler> clients) throws IOException {
    if (clients != null) {
      for (ClientHandler clientHandler : clients) {
        if (getId() != clientHandler.getId()) {
          clientHandler.out.println("Message from " + getName() + ": " + received);
        }
      }
    }
    System.out.println("Message from " + getName() + ": " + received);
  }
}//end ClientHandler class
